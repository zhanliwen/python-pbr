Name:           python-pbr
Version:        5.4.5
Release:        1
Summary:        Python Build Reasonableness
License:        ASL 2.0
URL:            http://pypi.python.org/pypi/pbr
Source0:        https://pypi.io/packages/source/p/pbr/pbr-%{version}.tar.gz

BuildArch:      noarch

%description
PBR is a library that injects some useful and sensible default behaviors into
your setuptools run. It started off life as the chunks of code that were copied
between all of the OpenStack projects. Around the time that OpenStack hit 18
different projects each with at least 3 active branches, it seems like a good
time to make that code into a proper re-usable library.

%package -n python3-pbr
Summary:        Python Build Reasonableness
%{?python_provide:%python_provide python3-pbr}

BuildRequires:  python3-devel python3-setuptools
Requires:       python3-setuptools
Requires:       git-core
Conflicts:      python2-%{pypi_name} < 5.4.3-2
%description -n python3-pbr
Manage dynamic plugins for Python applications

%prep
%autosetup -n pbr-%{version} -p1

rm -rf {test-,}requirements.txt pbr.egg-info/requires.txt


%build
export SKIP_PIP_INSTALL=1
%py3_build

%install
%py3_install
rm -rf %{buildroot}%{python3_sitelib}/pbr/tests
mv %{buildroot}%{_bindir}/pbr %{buildroot}%{_bindir}/pbr-3
ln -s ./pbr-3 %{buildroot}%{_bindir}/pbr
 
%if 0%{?do_test}
%check
%{__python3} setup.py test
%endif

%files -n python3-pbr
%license LICENSE
%doc README.rst
%{_bindir}/pbr
%{_bindir}/pbr-3
%{python3_sitelib}/*.egg-info/
%{python3_sitelib}/pbr/

%changelog
* Wed Jul 29 2020 jinzhimin<jinzhimin2@huawei.com> - 5.4.5-1
- update to 5.4.5 

* Fri Feb 21 2020 Wanjiankang <wanjiankang@huawei.com> - 4.1.1-3
- Modify BuildRequires.

* Fri Nov 22 2019 Wanjiankang <wanjiankang@huawei.com> - 4.1.1-2
- Initial package.
